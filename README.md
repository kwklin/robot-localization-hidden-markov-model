# Robot Localization - Hidden Markov Model

Given a robot, which consists of 4 noisy sensors that detects walls on each of the four directions, and cannot 100% go to an intended direction, we need to find where the robot is created using a hidden markov model.

We will define the elements of a hidden markov model as follows:
- observations: 4-bits integer indicating which of the four directions have a wall, in the order of up, right, down and left. For example, 10 (1010 in binary) means there's a wall on top and below the robot.
- epsilon: Indicates the probability of a sensor giving a wrong result. 
- actions: has values in {0, 1, 2, 3}, indicating the next action being up, right, down or left. If it hits a wall, it remains at the original position.
- gamma: Indicates the probability of the robot moving to the correct direction
- _s<sub>k</sub>_: State at time _k_
- _o<sub>k</sub>_: Observation at time _k_

## Filtering

To find the probability _P(S<sub>k</sub>|o<sub>0:k</sub>)_, we will perform the filtering calculation.

### Base Case:
 _f<sub>0:0</sub> = \alphaP(o<sub>0</sub>|S<sub>0</sub>)P(S<sub>0</sub>)_

### Recursive Case:
 _f<sub>0:k</sub> = \alphaP(o<sub>k</sub>|S<sub>k</sub>)\ Σ<sub>s<sub>k-1</sub></sub>P(S<sub>k</sub>|s<sub>k-1</sub>)f<sub>0:k-1</sub>_

## Smoothing

To find the probability _P(S<sub>k</sub>|o<sub>0:t</sub>)_, where _0 $\leq$ k $\leq$ t - 1_ we will perform the smoothing calculation.

### Base Case: 
_b<sub>t:(t-1)</sub> = 1_

### Recursive Case: 
_b<sub>(k+1)(t-1)</sub> = Σ<sub>s<sub>k+1</sub></sub>P(o<sub>k+1</sub>|s<sub>k+1</sub>)b<sub>(k+2):(t-1)</sub>P(s<sub>k+1</sub>|S<sub>k</sub>)_


By performing Filtering or Smoothing calculations, given observations, we can calculate the probaiblities of the robot being at any states, and return the argmax of it as it will be our estimate of where the robot actually is.

## Analysis
As Epsilon approaches 0 or 1, our robot is more confident about where it is, because when it's 0, it always returns the right detections, and if it's 1, it always returns the wrong detections, which in this case, we just need to flip all the bits around to have the correct detections
![Prob vs Epsilon](Images/MaxProbVSEpsilon.PNG "Prob vs Epsilon")

As Gamma increases, our robot is more confident about where it is, because it goes to the 1 direction we indicate, other than the rest of the 3 directions. As gamma decreases to 0, our robot has no idea where it actually moves to.
![Prob vs Gamma](Images/MaxProbVSGamma.PNG "Prob vs Gamma")

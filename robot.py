import numpy as np
from grid_env import GridEnvironment
import fba
from sklearn import preprocessing
import matplotlib.pyplot as plt

if __name__ == '__main__':

    # Specify width and height, in cells
    
    width = 16
    height = 4

    # Define which cell coordinates correspond to empty squares in which the robot can be
    empty_cell_coords = [[0,0],[0,1],[0,2],[0,3],[0,5],[0,6],[0,7],[0,8],[0,9],[0,11],[0,12],[0,13],[0,15],
                        [1,2],[1,3],[1,5],[1,8],[1,10],[1,12],
                        [2,1],[2,2],[2,3],[2,5],[2,8],[2,9],[2,10],[2,11],[2,12],[2,15],
                        [3,0],[3,1],[3,3],[3,4],[3,5],[3,7],[3,8],[3,9],[3,10],[3,12],[3,13],[3,14],[3,15]]
    
    n_empty_cells = len(empty_cell_coords)
    # Set the robot's initial location
    init_cell = 31
    # Set the robot's sensor error rate
    
    epsilons = [0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1]
    actions = [1,1,0,0,0,0,3,1,2,1]
    init_probs = [1 / n_empty_cells] * n_empty_cells
    max_average_prob = []
    for epsilon in epsilons:
        prob_dist = np.zeros((10, n_empty_cells))
        for trial in range (10):
            seed = trial
            env = GridEnvironment(width, height, empty_cell_coords, init_cell, epsilon, seed)
            obs = [env.reset()]
            for action in actions:  # perform actions
                obs.append(env.step(action))
            belief = fba.fba(env, actions, obs, init_probs)
            prob_dist[trial] = belief[-1,]
        mean_prob_dist = np.mean(prob_dist, axis = 0)
        max_average_prob.append(np.amax(mean_prob_dist))
    

    plt.plot(epsilons, max_average_prob)
    plt.xlabel("Epsilon")
    plt.ylabel("Maximum Probability")
    plt.title("Max Prob VS Epsilon")
    plt.show()
   
    epsilon = 0.2
    gammas = [0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1]
    actions = [1,1,0,0,0,0,3,1,2,1]
    init_probs = [1 / n_empty_cells] * n_empty_cells
    max_average_prob = []
    for gamma in gammas:
        prob_dist = np.zeros((11, n_empty_cells))
        for trial in range (10):
            seed = trial
            env = GridEnvironment(width, height, empty_cell_coords, init_cell, epsilon, seed, gamma)
            obs = [env.reset()]
            for action in actions:  # perform actions
                obs.append(env.step(action))
            belief = fba.fba(env, actions, obs, init_probs)
            prob_dist[trial] = belief[4,]
        mean_prob_dist = np.mean(prob_dist, axis = 0)
        max_average_prob.append(np.amax(mean_prob_dist))
    
    plt.plot(gammas, max_average_prob)
    plt.xlabel("Gamma")
    plt.ylabel("Maximum Probability")
    plt.title("Max Prob VS Gamma")
    plt.show()
    
     
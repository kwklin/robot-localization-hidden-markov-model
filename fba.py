import numpy as np
from typing import List, Dict
from grid_env import GridEnvironment
from sklearn import preprocessing

def forward_recursion(env: GridEnvironment, actions: List[int], observ: List[int], \
    probs_init: List[float]) -> np.ndarray:
    '''
    Perform forward recursion to calculate the filtering probabilities.

    Calculate and return a 2D numpy array containing the filtering probabilities 
    f_{0:0} to f_{0,t-1} where t = len(observ) is the number of time steps (t >= 1).

    :param env:         The environment.
        In the Marmoset tests, env.observe_matrix is None, and exactly one of 
        env.action_effects and env.transition_matrices is None.
    :param actions:     The actions for time steps 0 to t - 2.
    :param observ:      The observations for time steps 0 to t - 1.
    :param probs_init:  The prior probabilities of all the states at time step 0.

    :return: A 2D numpy array with shape (t, N) (N is the number of states)
        the k'th row represents the values of f_{0:k} (0 <= k <= t - 1).
    '''
    
    assert len(actions) == len(observ) - 1, \
            "There should be n - 1 actions for n time steps."

    '''The below code is to allow this function to run, please remove or modify to complete the implementation'''
    f_values = np.zeros((len(observ), env.n_states))
    trans_probs = env.trans_probs
    obs_probs = env.obs_probs
    for s in range(env.n_states):
        f_values[0,s] = obs_probs[s,observ[0]] * probs_init[s]
    f_values = preprocessing.normalize(f_values,norm="l1")
    for k in range(1,len(observ)):
        for s_k in range(env.n_states):
            sum = 0
            for s_km1 in range(env.n_states):
                sum = sum + trans_probs[actions[k - 1], s_km1, s_k] * f_values[k - 1, s_km1]
            f_values[k,s_k] = obs_probs[s_k, observ[k]] * sum
        f_values = preprocessing.normalize(f_values,norm="l1")
    return f_values


def backward_recursion(env: GridEnvironment, actions: List[int], observ: List[int] \
    ) -> np.ndarray:
    '''
    Perform backward recursion.

    Calculate and return a 2D numpy array containing the backward recursion messages
    b_{1:t-1} to b_{t:t-1} where t = len(observ).

    :param env:         The environment.
        In the Marmoset tests, env.observe_matrix is None, and exactly one of 
        env.action_effects and env.transition_matrices is None.    
    :param actions:     The actions for time steps 0 to t - 2.
    :param observ:      The observations for time steps 0 to t - 1.

    :return: A 2D numpy array with shape (t, N) (N is the number of states)
        the k'th row represents the values of b_{k+1:t-1} (0 <= k <= t - 1).
    '''
    
    assert len(actions) == len(observ) - 1, \
            "There should be n - 1 actions for n time steps."
               
    '''The below code is to allow this function to run, please remove or modify to complete the implementation'''
    b_values = np.zeros((len(observ), env.n_states))
    trans_probs = env.trans_probs
    obs_probs = env.obs_probs
    # b_values[1 to t,] for obs 0 to t-1
    for s in range(env.n_states):
        b_values[-1,s] = 1
    # k + 1 = len(observ) - 1 to 1
    # k = len(observ) - 2 to 0
    # the 0th col corresponds to the prob at time 1
    for k in range(len(observ) - 2, -1, -1):
        for s_k in range(env.n_states): # prob of bkp1 at o - 1 given o to t-1
            sum = 0
            for s_kp1 in range(env.n_states):
                sum = sum + obs_probs[s_kp1, observ[k + 1]] * b_values[k + 1, s_kp1] * trans_probs[actions[k], s_k, s_kp1]
            b_values[k,s_k] = sum
    return b_values


def fba(env: GridEnvironment, actions: List[int], observ: List[int], \
    probs_init: List[float]) -> np.ndarray:
    '''
    Execute the forward-backward algorithm. 

    Calculate and return a 2D numpy array containing the smoothed probabilities.
    The k'th row represents the smoothed probability distribution over all the states at time step k.

    :param env:         The environment.
        In the Marmoset tests, env.observe_matrix is None, and exactly one of 
        env.action_effects and env.transition_matrices is None.
    :param actions:     The actions for time steps 0 to t - 2.
    :param observ:      The observations for time steps 0 to t - 1.
    :param probs_init:  The prior probabilities of all the states at time step 0.

    :return: A 2D numpy array with shape (t, N) (N is the number of states)
        the k'th row represents the smoothed probabilities for time step k (0 <= k <= t - 1).
    '''
    
    f_values = forward_recursion(env, actions, observ, probs_init)
    b_values = backward_recursion(env, actions, observ)
    fb_values = np.zeros((len(observ), env.n_states))
    for k in range(len(observ)):
        for s_k in range(env.n_states):
            fb_values[k, s_k] = f_values[k, s_k] * b_values[k, s_k]
    fb_values = preprocessing.normalize(fb_values,norm="l1")
    return  fb_values

